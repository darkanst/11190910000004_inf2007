
package id.darkanst.AplikasiRestoran.model;



import java.time.LocalDateTime;
import java.math.BigDecimal;
import java.util.Objects;
import java.io.Serializable;

public class AplikasiRestoran implements Serializable {
    
    private static final long serialVersionUID = -6756463875294313469L;

    private String nama_menu;
    private String request;
    private String nama;
    private String nomor_order;
    private int jumlah_pesanan;
    private int code_order;
    private LocalDateTime waktu_pemesanan;
    private BigDecimal Total_Harga;
    private boolean keluar = false;

    public AplikasiRestoran (){
    
}
    public AplikasiRestoran(String nama_menu, String Request, String nama, 
            String nomor_order, int jumlah_pesanan, LocalDateTime waktu_pemesanan, 
            BigDecimal Total_Harga, int code_order){
        this.nama = nama;
        this.Total_Harga = Total_Harga;
        this.jumlah_pesanan = jumlah_pesanan;
        this.request = request;
        this.nama_menu = nama_menu;
        this.nomor_order = nomor_order;
        this.waktu_pemesanan = waktu_pemesanan;
        this.code_order = code_order;
        
    }

    public int getCode_order() {
        return code_order;
    }

    public void setCode_order(int code_order) {
        this.code_order = code_order;
    }
    
    public String getNama_menu() {
        return nama_menu;
    }

    public void setNama_menu(String nama_menu) {
        this.nama_menu = nama_menu;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomor_order() {
        return nomor_order;
    }

    public void setNomor_order(String nomor_order) {
        this.nomor_order = nomor_order;
    }

    public int getJumlah_pesanan() {
        return jumlah_pesanan;
    }

    public void setJumlah_pesanan(int jumlah_pesanan) {
        this.jumlah_pesanan = jumlah_pesanan;
    }

    public LocalDateTime getWaktu_pemesanan() {
        return waktu_pemesanan;
    }

    public void setWaktu_pemesanan(LocalDateTime waktu_pemesanan) {
        this.waktu_pemesanan = waktu_pemesanan;
    }

    public BigDecimal getTotal_Harga() {
        return Total_Harga;
    }

    public void setTotal_Harga(BigDecimal Total_Harga) {
        this.Total_Harga = Total_Harga;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }
    
    @Override
    public String toString() {
        return "NEROCAFE{" + "ORDER CODE = " + nomor_order + ", DATE = " + waktu_pemesanan 
                +", GUEST NAME = " + nama + ", ORDER MENU = " +jumlah_pesanan+" "
                + nama_menu + ", REQUEST = " + request + ", PRICE = " + Total_Harga + ", EXIT=" + keluar + '}';
    
}

   
}

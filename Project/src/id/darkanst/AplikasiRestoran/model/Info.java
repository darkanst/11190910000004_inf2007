
package id.darkanst.AplikasiRestoran.model;


public class Info {
    private final String aplikasi = "Aplikasi Restoran Nerocafe";
    private final String versi = "Demo Version";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersi() {
        return versi;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.darkanst.AplikasiRestoran.Controller;

import id.darkanst.AplikasiRestoran.model.Info;
import java.util.Scanner;

/**
 *
 * @author Darkan
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersi());
        System.out.println("=================================================");

        System.out.println("-----------------{OPTION PANE}-------------------");
        System.out.println("1. Menu Orders");
        System.out.println("2. Pay Billing");
        System.out.println("3. Daily Report");
        System.out.println("4. Exit ");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");
        
           do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
}

    private void setPilihMenu() {
        RestoranController app = new RestoranController();
        switch (noMenu) {
            case 1:
                app.setPemesananMenu();
                break;
            case 2:app.setPembayaranMenu();
                break;
            case 3:
                app.getDailyReports();
                break;
            case 4 :
                System.out.println("NEROCAFE SAYING GOOD BYE TO YOU....");
                System.exit(0);
                break;
            default :
                while(noMenu>4){
                    getMenuAwal();
                }
            break;

        }
    }
}

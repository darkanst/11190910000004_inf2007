package id.darkanst.AplikasiRestoran.Controller;

import id.darkanst.AplikasiRestoran.model.AplikasiRestoran;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RestoranController {

    private static final String FILE = "C:\\Users\\Darkan\\Documents\\Gitlab darkanst\\11190910000004_inf2007\\Project\\lib\\aplikasirestoran.json";
    private AplikasiRestoran aplikasirestoran;
    private final DateTimeFormatter dateTimeFormat;
    private final Scanner in;
    private String nama_menu;
    private String request;
    private String nama;
    private String nomor_order;
    private int jumlah_pesanan;
    private final LocalDateTime waktu_pemesanan;
    private BigDecimal Total_Harga;
    private int pilihan;
    private int code_order;

    public RestoranController() {
        in = new Scanner(System.in);
        waktu_pemesanan = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("dd-MM-yyy | HH:mm");

    }

    public void setPemesananMenu() {
        System.out.println("==========================================================");
        System.out.println("                       {NEROCAFE MENU}                   ");
        System.out.println("==========================================================");
        System.out.println("1. Australian Croissant                        Rp. 28.000");
        System.out.println("2. Alphen Danish Ganache                       Rp. 50.000");
        System.out.println("3. Algerian Pizza Hot                          Rp. 50.000");
        System.out.println("4. Blueberry Pastry w/ Raspberry Liquor        Rp. 100.000");
        System.out.println("5. Bagel Prawn Sakura Vegas                    Rp. 120.000");
        System.out.println("6. Curry Indian Puff W/ Vegas                  Rp. 150.000");
        System.out.println("7. Cream Pastry Soup [Prawn|Corn|Mushroom]     Rp. 110.000");
        System.out.println("==========================================================");
        System.out.println("8. Double S/ Espresso           [ C | H ]      Rp. 50.000");
        System.out.println("9. Cappucino                    [ C | H ]      Rp. 75.000");
        System.out.println("10. Caffe Americano             [ C | H ]      Rp. 25.000");
        System.out.println("11. Vani/Cara/Haze Latte        [ C | H ]      Rp. 80.000");
        System.out.println("12. Signature Chocolate         [ C | H ]      Rp. 75.000");
        System.out.println("13. Chocolate Latte             [ C | H ]      Rp. 67.000");
        System.out.println("14. Aust/Arbn/Java Brew Coffe   [ C | H ]      Rp. 120.000");

        System.out.println("Order Number : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Order Number : ");
        }
        nomor_order = in.next();

        String formatwaktu_pemesanan = waktu_pemesanan.format(dateTimeFormat);
        System.out.println("Time Order : " + formatwaktu_pemesanan);

        System.out.println("Guest Name : ");
        nama = in.next();

        System.out.println("Orders : ");
        nama_menu = in.next();
        System.out.println("Code Orders : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Code Orders : ");
        }
        code_order = in.nextInt();
        if(code_order > 14){
            System.out.println("There's no menu in that code!" + "\n" + "Code Orders : ");
            code_order = in.nextInt();
        }
        System.out.println("How many do you want to take? : ");
        jumlah_pesanan = in.nextInt();
        System.out.println("Any Request for your order?");
        request = in.next();
        aplikasirestoran = new AplikasiRestoran();
        aplikasirestoran.setNomor_order(nomor_order);
        aplikasirestoran.setWaktu_pemesanan(waktu_pemesanan);
        aplikasirestoran.setNama(nama.toUpperCase());
        aplikasirestoran.setNama_menu(nama_menu.toUpperCase());
        aplikasirestoran.setCode_order(code_order);
        aplikasirestoran.setJumlah_pesanan(jumlah_pesanan);
        aplikasirestoran.setRequest(request.toUpperCase());

        setWriteAplikasiRestoran(FILE, aplikasirestoran);

        System.out.println("is this enough? are you need anything else?");
        System.out.println("1. Yes, I want to order again. | 2. No, Thanks.");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            System.out.println("=======================================================================");
            System.out.println("No Order    : " + aplikasirestoran.getNomor_order());
            System.out.println("DATE        : " + aplikasirestoran.getWaktu_pemesanan());
            System.out.println("Guest Name  : " + aplikasirestoran.getNama());
            System.out.println("Order Name  : " + aplikasirestoran.getJumlah_pesanan() + " " + aplikasirestoran.getNama_menu());
            System.out.println("REQ         : " + aplikasirestoran.getRequest());
            System.out.println("STATUS      : DELIVERED TO KITCHEN");
            System.out.println("=======================================================================");
            Menu apx = new Menu();
            apx.getMenuAwal();
        } else {
            setPemesananMenu();
        }
    }

    public void setWriteAplikasiRestoran(String file, AplikasiRestoran aplikasirestoran) {
        Gson gson = new Gson();

        List<AplikasiRestoran> resto = getReadAplikasiRestoran(file);
        resto.remove(aplikasirestoran);
        resto.add(aplikasirestoran);

        String json = gson.toJson(resto);
        try {
            FileWriter writer;
            writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(RestoranController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setPembayaranMenu() {
        System.out.println("Orders Number : ");
        nomor_order = in.next();

        AplikasiRestoran a = getSearch(nomor_order);
        if (a != null) {
            if (a.getJumlah_pesanan() > 1) {
                Total_Harga = new BigDecimal(a.getJumlah_pesanan());
                if (a.getCode_order() == 1) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(28000));
                } else if (a.getCode_order() == 2) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(50000));
                } else if (a.getCode_order() == 3) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(50000));
                } else if (a.getCode_order() == 4) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(100000));
                } else if (a.getCode_order() == 5) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(120000));
                } else if (a.getCode_order() == 6) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(150000));
                } else if (a.getCode_order() == 7) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(110000));
                } else if (a.getCode_order() == 8) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(50000));
                } else if (a.getCode_order() == 9) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(75000));
                } else if (a.getCode_order() == 10) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(25000));
                } else if (a.getCode_order() == 11) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(80000));
                } else if (a.getCode_order() == 12) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(75000));
                } else if (a.getCode_order() == 13) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(67000));
                } else if (a.getCode_order() == 14) {
                    Total_Harga = Total_Harga.multiply(new BigDecimal(120000));
                }else{
                    Total_Harga = Total_Harga.multiply(new BigDecimal(0));
                }
            } else {
                jumlah_pesanan = 1;
                if (a.getCode_order() == 1) {
                    Total_Harga = new BigDecimal(28000);
                } else if (a.getCode_order() == 2) {
                    Total_Harga = new BigDecimal(50000);
                } else if (a.getCode_order() == 3) {
                    Total_Harga = new BigDecimal(50000);
                } else if (a.getCode_order() == 4) {
                    Total_Harga = new BigDecimal(100000);
                } else if (a.getCode_order() == 5) {
                    Total_Harga = new BigDecimal(120000);
                } else if (a.getCode_order() == 6) {
                    Total_Harga = new BigDecimal(150000);
                } else if (a.getCode_order() == 7) {
                    Total_Harga = new BigDecimal(110000);
                } else if (a.getCode_order() == 8) {
                    Total_Harga = new BigDecimal(50000);
                } else if (a.getCode_order() == 9) {
                    Total_Harga = new BigDecimal(75000);
                } else if (a.getCode_order() == 10) {
                    Total_Harga = new BigDecimal(25000);
                } else if (a.getCode_order() == 11) {
                    Total_Harga = new BigDecimal(80000);
                } else if (a.getCode_order() == 12) {
                    Total_Harga = new BigDecimal(75000);
                } else if (a.getCode_order() == 13) {
                    Total_Harga = new BigDecimal(67000);
                } else if (a.getCode_order() == 14) {
                    Total_Harga = new BigDecimal(120000);
                }else{
                    Total_Harga = new BigDecimal(0);
                }
            }

            a.setTotal_Harga(Total_Harga);
            a.setKeluar(true);

            System.out.println("=======================================================================");
            System.out.println("No Order    : " + a.getNomor_order());
            System.out.println("DATE        : " + a.getWaktu_pemesanan());
            System.out.println("Guest Name  : " + a.getNama());
            System.out.println("Order Name  : " + a.getJumlah_pesanan() + " " + a.getNama_menu());
            System.out.println("REQ         : " + a.getRequest());
            System.out.println("STATUS      : SERVED");
            System.out.println("PRICE       : Rp. " + Total_Harga);
            System.out.println("=======================================================================");

            System.out.println("Are you wanna pay the bills?");
            System.out.print("1) Yes, 2) Later, 3) Exit");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWriteAplikasiRestoran(FILE, a);
                    break;
                case 2:
                    setPembayaranMenu();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Are you want to payment others bills?");
            System.out.println("1) Yes , 2) No : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setPembayaranMenu();
            }

        } else {
            System.out.println("ERROR : WE CAN'T FOUND DATA GUEST");
            setPembayaranMenu();
        }
    }

    public AplikasiRestoran getSearch(String nomor_order) {
        List<AplikasiRestoran> resto = getReadAplikasiRestoran(FILE);

        AplikasiRestoran a = resto.stream()
                .filter(aa -> nomor_order.equalsIgnoreCase(aa.getNomor_order()))
                .findAny()
                .orElse(null);

        return a;

    }

    public List<AplikasiRestoran> getReadAplikasiRestoran(String file) {
        List<AplikasiRestoran> resto = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader x = new FileReader(file); BufferedReader read = new BufferedReader(x)) {
            while ((line = read.readLine()) != null) {
                AplikasiRestoran[] res = gson.fromJson(line, AplikasiRestoran[].class);
                resto.addAll(Arrays.asList(res));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestoranController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestoranController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resto;
    }

    public void getDailyReports() {
        List<AplikasiRestoran> resto = getReadAplikasiRestoran(FILE);
        Predicate<AplikasiRestoran> isKeluar = e -> e.isKeluar() == true;
        Predicate<AplikasiRestoran> isWaktu_pemesanan = e -> e.getWaktu_pemesanan().toLocalDate().equals(LocalDate.now());

        List<AplikasiRestoran> aResults = resto
                .stream().filter(isKeluar.and(isWaktu_pemesanan)).collect(Collectors.toList());
        BigDecimal total = aResults.stream()
                .map(AplikasiRestoran::getTotal_Harga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        aResults.forEach((a) -> {
            System.out.println( "\n" +"======================================================================="
                    + "\n" +"No Order    : " + a.getNomor_order()
                    + "\n" +"DATE        : " + a.getWaktu_pemesanan()
                    + "\n" +"Guest Name  : " + a.getNama()
                    + "\n" +"Order Name  : " + a.getJumlah_pesanan() + " " + a.getNama_menu()
                    + "\n" +"REQ         : " + a.getRequest()
                    + "\n" +"STATUS      : SERVED"
                    + "\n" +"PRICE       : Rp. " + a.getTotal_Harga()
                    + "\n" +"=======================================================================");
        });
        System.out.println("                                                                                                                           ");
        System.out.println("======================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("======================================");

        System.out.println("Are you want to refresh this report?");
        System.out.println("1) Ya , 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDailyReports();

        }

    }

}

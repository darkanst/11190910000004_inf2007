
package id.darkanst.pertemuan.kedua;
// Programs by Ahmad Yani
import java.util.Scanner;
public class SuhuRata {
    public static void main(String[] args) {
        int Suhumin, Suhumax, Suhurata;
        Scanner in =new Scanner(System.in);
        System.out.println("Masukan Suhu Minimal");
        Suhumin = in.nextInt(); 
        System.out.println("Masukan Suhu Maksimal");
        Suhumax = in.nextInt();   
        Suhurata = (Suhumax + Suhumin)/2;
        
        System.out.println("Suhu minimal " + Suhumin);
        System.out.println("Suhu maksimal " + Suhumax);
        System.out.println("Suhu rata - rata dalam sehari" + " " +Suhurata);
    }
}

package id.darkanst.pertemuan.ketiga;

import java.util.Scanner;
public class KonversiJarak {
    public static void main(String[] args) {
        int JarakX, km, m, cm, SisaJarak, SatuMeter = 100, SatuKilometer = 100000;
        
        Scanner jarak = new Scanner(System.in);
        System.out.println("Masukkan jarak :");
        JarakX = jarak.nextInt();
        
        km = JarakX / SatuKilometer;
        SisaJarak = JarakX % SatuKilometer;
        m = SisaJarak / SatuMeter;
        SisaJarak = SisaJarak % SatuMeter;
        cm = SisaJarak;
        
        System.out.println("Jarak yang ditempuh semut adalah " + km + " Km " + m + " M " + cm + "Cm");
    }
}

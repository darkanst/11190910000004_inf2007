package id.darkanst.pertemuan.ketiga;

import java.util.Scanner;
public class SelisihTanggal {

    public static void main(String[] args) {
        int Tanggal1, Tanggal2, Tanggal3, Bulan1, Bulan2, Bulan3, Tahun1, Tahun2, Tahun3, 
            TotalHari1, TotalHari2, SelisihHari, Sisa;

        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Tanggal 1 ");
        Tanggal1 = in.nextInt();
        System.out.println("Masukkan Bulan 1 ");
        Bulan1 = in.nextInt();
        System.out.println("Masukkan Tahun 1 ");
        Tahun1 = in.nextInt();  
        System.out.println("Masukkan Tanggal 2 ");
        Tanggal2 = in.nextInt();
        System.out.println("Masukkan Bulan 2 ");
        Bulan2 = in.nextInt();
        System.out.println("Masukkan Tahun 2 ");
        Tahun2 = in.nextInt();
        System.out.println("Tanggal ke 1 = " + Tanggal1 + "-" + Bulan1 + "-" + Tahun1);
        System.out.println("Tanggal ke 2 = " + Tanggal2 + "-" + Bulan2 + "-" + Tahun2);
        
        TotalHari1 = (Tahun1*365) + (Bulan1*30) + Tanggal1;
        TotalHari2 = (Tahun2*365) + (Bulan2*30) + Tanggal2;
        SelisihHari = TotalHari2 - TotalHari1;
        System.out.println("Selisih Hari dari kedua tanggal tersebut adalah " + SelisihHari);

        Tahun3 = SelisihHari / 365;
        Sisa = SelisihHari % 365;
        Bulan3 = Sisa / 30;
        Tanggal3 = Sisa % 30;
        System.out.println(Tahun3 + " Tahun " + Bulan3 + " Bulan " + Tanggal3 + " Hari ");
    }
}

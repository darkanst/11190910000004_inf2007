package id.darkanst.pertemuan.ketiga;

import java.util.Scanner;
public class NilaiTukaran {
    public static void main(String[] args) {
        int Pecahan1 = 1000, Pecahan2 = 500, Pecahan3 = 100, Pecahan4 = 50, Pecahan5 = 25, 
            JumlahPecahan1, JumlahPecahan2, JumlahPecahan3, JumlahPecahan4, JumlahPecahan5, 
            NilaiUang, Sisa;
        
        Scanner Uang = new Scanner(System.in);
        
        System.out.println("Masukkan nilai uang dalam kelipatan 25 :");
        NilaiUang = Uang.nextInt();
        JumlahPecahan1 = NilaiUang / Pecahan1;
        Sisa = NilaiUang % Pecahan1;
        JumlahPecahan2 = Sisa / Pecahan2;
        Sisa = NilaiUang % Pecahan2;
        JumlahPecahan3 = Sisa / Pecahan3;
        Sisa = NilaiUang % Pecahan3;
        JumlahPecahan4 = Sisa / Pecahan4;
        Sisa = NilaiUang % Pecahan4;
        JumlahPecahan5 = Sisa / Pecahan5;
        
        System.out.println("Jumlah Pecahan Rp 1000 ada " + JumlahPecahan1 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 500 ada " + JumlahPecahan2 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 100 ada " + JumlahPecahan3 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 50 ada " + JumlahPecahan4 + " pecahan");
        System.out.println("Jumlah Pecahan Rp 25 ada " + JumlahPecahan5 + " pecahan");
        
    }
}

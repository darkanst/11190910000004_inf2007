
package id.darkanst.pertemuan.keempat;

import java.util.Scanner;


public class KonversiAngkaKeTeks {
    public static void main(String[] args) {
        int angka;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Angka");
        angka = in.nextInt();
        
        switch(angka){
            case 1:
                System.out.println("satu");
                break;
            case 2:
                System.out.println("dua");
                break;
            case 3:
                System.out.println("tiga");
                break;
            case 4:
                System.out.println("empat");
                break;
            default:
                System.out.println("Angka yang dimasukan salah!");
        }
    }
}

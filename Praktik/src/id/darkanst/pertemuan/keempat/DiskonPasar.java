
package id.darkanst.pertemuan.keempat;
import java.util.Scanner;
public class DiskonPasar {
    public static void main(String[] args) {
        int NilaiBelanja, TotalBelanja, Diskon;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Total Belanjaan Anda...");
        TotalBelanja = in.nextInt();
        
        if(TotalBelanja > 120000){
            Diskon = TotalBelanja *7 /100;
            NilaiBelanja = TotalBelanja - Diskon;
            System.out.println("Anda Mendapatkan Diskon Sebesar.."+" " +Diskon + "\n" + "Jumlah yang Harus Anda Bayar Senilai" + " "+ NilaiBelanja);
        }else{
            NilaiBelanja = TotalBelanja;
            System.out.println("Anda Tidak Mendapatkan Diskon" + "\n" + "Jumlah yang Harus Anda Bayar Senilai" + " "+ NilaiBelanja);
        }
        
    }
}

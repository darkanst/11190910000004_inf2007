
package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;
public class AplikasiPenambahanMatriks {
   public static void main(String[] args) {

        int i, j, nBaris, nKolom;

        PenambahanMatriks x = new PenambahanMatriks();
        Scanner in = new Scanner(System.in);

        System.out.print("Masukkan Jumlah Baris: ");
        nBaris = in.nextInt();
        System.out.print("Masukkan Jumlah Kolom: ");
        nKolom = in.nextInt();

        int[][] A = new int[nBaris][nKolom];
        int[][] B = new int[nBaris][nKolom];
        int[][] C = new int[nBaris][nKolom];

        System.out.println("Input nilai Array A");
        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                System.out.print("Masukkan Array A [" + i + "," + j + "] : ");
                A[i][j] = in.nextInt();
            }
        }

        System.out.println("\nInput nilai Array B");
        for (i = 0; i < B.length; i++) {
            for (j = 0; j < B[i].length; j++) {
                System.out.print("Masukkan Array B [" + i + "," + j + "] : ");
                B[i][j] = in.nextInt();
            }
        }
        x.getPenambahanMatriks(A, B, nBaris, nKolom);
    }
} 

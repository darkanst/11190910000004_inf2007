package id.darkanst.pertemuan.kedelapan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ContohCollection {

    public static void main(String[] args) {
        System.err.println("--------List-------");
        List namaList = new ArrayList();
        namaList.add(1);
        namaList.add(1);
        namaList.add(2);
        for (int i = 0; i < namaList.size(); i++) {
            System.out.println(namaList.get(i));
        }
        System.out.println("---Set---");
        Set namaSet = new HashSet();
        namaSet.add(3);
        namaSet.add(8);
        namaSet.add(2);
        for (Object o : namaSet) {
            System.out.println(o.toString());
        }

        System.err.println("--- Map ---");
        Map namaMap = new HashMap();
        namaMap.put(0, "Ahmad");
        namaMap.put(1, "Fitroh");
        namaMap.put(2, "Zaki");

        namaMap.forEach((k, v) -> {
            System.out.println("Key : " + k + " Value: " + v);
        });
    }
}

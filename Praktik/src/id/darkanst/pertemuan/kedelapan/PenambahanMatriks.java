
package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;
public class PenambahanMatriks {
    int i, j;

    public int[][] getPenambahanMatriks(int A[][], int B[][], int nBaris, int nKolom) {
        Scanner in = new Scanner(System.in);
        int[][] C = new int[nBaris][nKolom];

        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }

        System.out.println("");
        System.out.println("Hasil Penjumlahan Array");
        for (i = 0; i < C.length; i++) {
            for (j = 0; j < C[i].length; j++) {
                System.out.print(C[i][j] + "  ");
            }
            System.out.println("");
        }
        return C;
    }
}

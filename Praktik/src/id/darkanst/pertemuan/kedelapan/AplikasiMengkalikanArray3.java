
package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;


public class AplikasiMengkalikanArray3 {
    public static void main(String[] args) {
        int nilai[][] = new int[3][5];
        int i, j;
        MengkalikanArray3 x = new MengkalikanArray3();
        Scanner in = new Scanner(System.in);

        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print("Masukan Array Yang Akan Dikalikan [" + i + "," + j + "] : ");
                nilai[i][j] = in.nextInt();
            }
        }

        x.getInfo(nilai);
        x.getKali(nilai);
        x.getHasil(nilai);
    }
}


package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;


public class AplikasiArrayMaksimum {
    public static void main(String[] args) {

        int i, n;

        ArrayMaksimum x = new ArrayMaksimum();
        Scanner in = new Scanner(System.in);

        System.out.print("Jumlah Array = ");
        n = in.nextInt();

        int A[] = new int[n];

        for (i = 0; i < A.length; i++) {
            System.out.print("Masukkan Array [" + i + "] : ");
            A[i] = in.nextInt();
        }
        System.out.println("Array Maksimum Adalah = " + x.getMaks(A, n));
    }
}

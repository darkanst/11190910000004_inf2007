
package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;

public class AplikasiMencariNilaiXArray {
    public static void main(String[] args) {
        int angka, jumlah, i, j;
        Scanner in = new Scanner(System.in);
        MencariNilaiXArray In = new MencariNilaiXArray();

        System.out.print("Jumlah Array = ");
        jumlah = in.nextInt();
        int[] nilai = new int[jumlah];

        for (i = 0; i < nilai.length; i++) {
            System.out.print("Masukkan Array [" + i + "] : ");
            nilai[i] = in.nextInt();
        }

        System.out.print("Number You Want To Search? \n");
        angka = in.nextInt();

        In.getCari(nilai, angka);
    }
}



package id.darkanst.pertemuan.kedelapan;

import java.util.Scanner;

public class AplikasiArrayTerkecil {
  public static void main(String[] args) {
        int i, n;
        Scanner in = new Scanner (System.in);
        ArrayTerkecil x = new ArrayTerkecil();
        
        System.out.print("N = ");
        n = in.nextInt();
        
        int[] A = new int[n];
        
        for (i = 0; i < A.length; i++) {
            System.out.print("Masukkan Angka Array [" + i + "]: ");
            A[i] = in.nextInt();
        }
        x.getTerkecil(A, n);
    }
}

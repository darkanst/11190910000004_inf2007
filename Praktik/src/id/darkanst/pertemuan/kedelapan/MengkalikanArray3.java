
package id.darkanst.pertemuan.kedelapan;

public class MengkalikanArray3 {
    int i, j;

    public void getInfo(int nilai[][]) {
        System.out.println("Matriks Sebelum di Kalikan");
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print(nilai[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public int[][]getKali(int nilai[][]) {
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                nilai[i][j] = nilai[i][j] * 3;
            }
        }
        return nilai;
    }

    public void getHasil(int nilai[][]) {
        System.out.println("Hasil Perkalian Matriks");
        for (i = 0; i < nilai.length; i++) {
            for (j = 0; j < nilai[i].length; j++) {
                System.out.print(nilai[i][j] + " ");
            }
            System.out.println("");
        }
    }
}

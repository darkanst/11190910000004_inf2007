
package id.darkanst.pertemuan.kesembilan;

import java.util.Scanner;

public class PencarianLain {

    public int getPencarianLain (int A[], int x) {
        int i, nilai = -1;
        
        for (i = 0; i < A.length; i++) {
            if (A[i] == x) {
                nilai = i;
            }           
        }
        return nilai;
    }
    
    public static void main(String[] args) {
        int[] A = {9, 6, 12, 31, 62, 77};
        int x, n;
        
        Scanner in = new Scanner(System.in);
        PencarianLain y = new PencarianLain();
        
        System.out.print("Berapa Nilai Yang Ingin Anda Cari? ");
        x = in.nextInt();
        
        System.out.println("idx = " + y.getPencarianLain(A, x));
    }
}


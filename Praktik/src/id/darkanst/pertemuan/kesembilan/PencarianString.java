
package id.darkanst.pertemuan.kesembilan;

import java.util.Scanner;

public class PencarianString {
    
    public int getStringSequentialSearch(String A[], int n, String x) {
        int i;

        i = 0;
        while ((i < n - 1) && (!(A[i].equals(x)))) {
            i = i + 1;
        }
        if (A[i].equals(x)) {
            return i;
        } else {
            return -1;
        }
    }
    
    public int getStringBinarySearch(String A[], int n, String x) {
        int i, j, k = 0;
        boolean ketemu = false;

        i = 0;
        j = n-1;
        

        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (A[k].equals(x)) {
                ketemu = true;
            } else {
                if (A[k].compareTo(x) < 0) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return 0;
        }
    }
    
    public static void main(String[] args) {
        String[] A = {"Jungkook", "Jeno", "Jaemin", "Jaehyun", "Yohan", "Junho"};
        int n = 6;
        String x;
        
        Scanner in = new Scanner(System.in);
        PencarianString y = new PencarianString();
        
        System.out.print("Nama Yang Ingin Anda Cari? ");
        x = in.next();
        
        System.out.println("idx = " + y.getStringSequentialSearch(A, n, x));
        System.out.println("idx = " + y.getStringBinarySearch(A, n, x));
    }
}

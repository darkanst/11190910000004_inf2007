package id.darkanst.pertemuan.kelima;

import java.util.Scanner;

public class Romawi {

    public static void main(String[] args) {
        int Bilangan = 0;

        Scanner in = new Scanner(System.in);

        while (Bilangan != -1) {
            System.out.println("\n" + "Input the number you want to conver...");
            Bilangan = in.nextInt();
            if (Bilangan > 0 && Bilangan < 100000) {
                while (Bilangan >= 1000) {
                    System.out.println("M");
                    Bilangan = Bilangan - 1000;

                }
                if (Bilangan > 500) {
                    if (Bilangan >= 900) {
                        System.out.println("CM");
                        Bilangan = Bilangan - 900;
                    } else {
                        System.out.println("D");
                        Bilangan = Bilangan - 500;
                    }
                }
                while (Bilangan >= 100) {
                    if (Bilangan >= 400) {
                        System.out.println("CD");
                        Bilangan = Bilangan - 400;
                    } else {
                        System.out.println("C");
                        Bilangan = Bilangan - 100;
                    }
                }
                if (Bilangan >= 50) {
                    if (Bilangan >= 90) {
                        System.out.println("XC");
                        Bilangan = Bilangan - 90;
                    } else {
                        System.out.println("L");
                        Bilangan = Bilangan - 50;
                    }
                }
                while (Bilangan >= 10) {
                    if (Bilangan >= 40) {
                        System.out.println("XL");
                        Bilangan = Bilangan - 40;
                    } else {
                        System.out.println("X");
                        Bilangan = Bilangan - 10;
                    }
                }
                if (Bilangan >= 5) {
                    if (Bilangan == 9) {
                        System.out.print("IX");
                        Bilangan = Bilangan - 9;
                    } else {
                        System.out.print("V");
                        Bilangan = Bilangan - 5;
                    }
                }
                while (Bilangan >= 1) {
                    if (Bilangan == 4) {
                        System.out.print("IV");
                        Bilangan = Bilangan - 4;
                    } else {
                        System.out.print("I");
                        Bilangan = Bilangan - 1;
                    }
                }
            } else {
                System.out.println("Not Responding");
            }
        }
    }
}


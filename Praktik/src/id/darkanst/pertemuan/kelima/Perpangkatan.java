package id.darkanst.pertemuan.kelima;

import java.util.Scanner;

public class Perpangkatan {

    public static void main(String[] args) {
        float a, p;
        int n, i;

        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        System.out.println("masukan nilai pangkat untuk mempangkatkan" + " " + n);
        a = in.nextFloat();

        p = 1;
        for (i = 1; i <= n; i++) {
            p = p * a;
        }
        System.out.println(p);

    }
}

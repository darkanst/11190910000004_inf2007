
package id.darkanst.pertemuan.kelima;

import java.util.Scanner;
public class Faktorial {
    public static void main(String[] args) {
         int n, faktor, i;
         
         faktor = 1;
         Scanner in = new Scanner(System.in);
         n = in.nextInt();
         
         for(i=1; i<=n; i++){
             faktor = faktor * i;
         }
         System.out.println(faktor);
    }
}

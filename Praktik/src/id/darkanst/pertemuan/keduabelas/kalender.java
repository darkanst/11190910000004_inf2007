package id.darkanst.pertemuan.keduabelas;

public class kalender {

    private int tanggal, bulan, tahun;

    public kalender(int tanggal) {
        this.bulan = 1;
        this.tanggal = tanggal;
        this.tahun = 2019;
    }

    public kalender(int bulan, int tahun) {
        this.bulan = bulan;
        this.tahun = tahun;
        this.tanggal = 1;
    }

    public kalender(int tanggal, int bulan, int tahun) {
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
    }
    
    

    public int getTanggal() {
        return tanggal;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public int getBulan() {
        return bulan;
    }

    public void setBulan(int bulan) {
        this.bulan = bulan;
    }

    public int getTahun() {
        return tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }
    

    }


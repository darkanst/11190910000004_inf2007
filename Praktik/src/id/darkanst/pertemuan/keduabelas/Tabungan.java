
package id.darkanst.pertemuan.keduabelas;


public class Tabungan  {
    protected int saldo;
    
    public Tabungan(int saldo){
        this.saldo = saldo;
    }
    
    public int ambilUang(int jumlah){
        saldo = saldo - jumlah;
        return saldo ;    
    }
}

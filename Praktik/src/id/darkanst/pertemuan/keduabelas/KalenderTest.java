package id.darkanst.pertemuan.keduabelas;

public class KalenderTest {
    public static String getTime(kalender kalender) {
        return kalender.getTanggal() + "-" + kalender.getBulan() + "-" + kalender.getTahun();
    }
    public static void main(String[] args) {
        kalender k = new kalender(8);
        System.out.println("Waktu awal : " + getTime(k));
        k.setTanggal(9);
        System.out.println("1 hari setelah waktu awal : " + getTime(k));
        k = new kalender(6, 2021);
        System.out.println("Waktu berubah : " +getTime(k));
        k.setBulan(7);
        System.out.println("1 bulan setelah itu : " +getTime(k));
        k = new kalender(20, 10, 2022);
        System.out.println("Waktu berubah : " +getTime(k));
        k.setTahun(2023);
        System.out.println("1 tahun setelah itu : " +getTime(k));
    }
}

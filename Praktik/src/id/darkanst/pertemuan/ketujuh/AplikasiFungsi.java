
package id.darkanst.pertemuan.ketujuh;

public class AplikasiFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi y = new Fungsi();
        
        System.out.println("___________________________");
        System.out.println("x                      f(x)");
        System.out.println("___________________________");
        
        x = (float)10.0;
        while(x<=15.0){
            System.out.println(x + "        " + y.f(x));
            x = x + (float)0.2;
        }
        System.out.println("_________________");
    }
}

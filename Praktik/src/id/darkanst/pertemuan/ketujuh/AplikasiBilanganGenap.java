
package id.darkanst.pertemuan.ketujuh;

import java.util.Scanner;
public class AplikasiBilanganGenap {
    public static void main(String[] args) {
        int bilangan;
        
        BilanganBulat x = new BilanganBulat();
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Bilangan!");
        bilangan = in.nextInt();
        
        if(x.getBil(bilangan)){
            System.out.println("Bilangan Genap");
        }else{
            System.out.println("Bilangan Ganjil");
        }
    }
}

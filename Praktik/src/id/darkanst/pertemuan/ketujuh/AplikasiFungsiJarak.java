
package id.darkanst.pertemuan.ketujuh;

import java.util.Scanner;

public class AplikasiFungsiJarak {
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        int x1, x2, y1, y2;
        
        System.out.println("Masukan x1");
        x1 = in.nextInt();
        System.out.println("Masukan x2");
        x2 = in.nextInt();
        System.out.println("Masukan y1");
        y1 = in.nextInt();
        System.out.println("Masukan y2");
        y2 = in.nextInt();
        
        FungsiJarak x = new FungsiJarak();
        
        System.out.println("Jarak titik pertama dengan titik kedua : " +x.FungsiJarak(x1,x2,y1,y2));
    }
}

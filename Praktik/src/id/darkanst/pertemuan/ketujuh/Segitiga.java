
package id.darkanst.pertemuan.ketujuh;

import java.util.Scanner;


public class Segitiga {
    public Segitiga(){
        double alas,tinggi,luas;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan Alas");
        alas = in.nextDouble();
        System.out.println("Masukan Tinggi");
        tinggi = in.nextDouble();
        luas = alas*tinggi/2;
        System.out.println("Luas : "+luas);
    }
}
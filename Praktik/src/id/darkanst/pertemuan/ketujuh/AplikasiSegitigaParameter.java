
package id.darkanst.pertemuan.ketujuh;

import java.util.Scanner;
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
       float alas; 
       float tinggi;
       int N, i;
       
        System.out.println("Banyak Segitiga?");
        N = in.nextInt();
        
        for(i=0; i<N; i++){
        System.out.println("Masukan Alas Segitiga");
       alas = in.nextFloat();
        System.out.println("Masukan Tinggi Segitiga");
       tinggi = in.nextFloat();
       SegitigaParameter x = new SegitigaParameter(alas, tinggi);
       x.getluas();
       
    }
}
}

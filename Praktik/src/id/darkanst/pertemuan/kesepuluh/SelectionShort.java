
package id.darkanst.pertemuan.kesepuluh;

import java.util.Arrays;


public class SelectionShort {
    public int[] getSelectionShortMaks(int L[], int n){
        int i,j,imaks,temp;
        
        for(i=n-1; i > 0; i--){
            imaks=0;
            for(j=1; j < i+1; j++){
                
                if(L[j] > L[imaks]){
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        
        return L;
        
    }
    public int[] getSelectionShortMin(int L[], int n){
        int i,j,imin,temp;
        
        for(i=1; i < n-1; i++){
            imin=i;
            for(j=i+1; j < n-1; j++){
                
                if(L[j] < L[imin]){
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25,27,10,8,76,21};
        SelectionShort b = new SelectionShort();
        int n = L.length;
        System.out.println(Arrays.toString(L));
        b.getSelectionShortMaks(L, n);
        System.out.println(Arrays.toString(L));
        b.getSelectionShortMin(L, n);
        System.out.println(Arrays.toString(L));
    }
}

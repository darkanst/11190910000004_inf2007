package id.darkanst.pertemuan.kesebelas;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
public class SortArsip {
    public int[] getSort(String Arsip, int n) {
        int[] L = new int[n];
        int i = 0, k, temp;
        Object object = null;

        try {
            BufferedReader file = new BufferedReader(new FileReader(Arsip));
            Scanner line = new Scanner(file);
            i = 0;
            while (line.hasNextInt()) {
                L[i] = line.nextInt();
                i++;
            }
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
            
            n = i;
        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int a, i, n;
        String Arsip = "C:\\Users\\Darkan\\Documents\\Gitlab darkanst\\temp\\ArsipPendek.txt";
        Scanner in = new Scanner(System.in);
        SortArsip app = new SortArsip();
        System.out.print("Input the Value of Number: ");
        n = in.nextInt();

        try {
            try (PrintWriter outFile = new PrintWriter(new FileOutputStream(Arsip))) {
                for (i = 1; i <= n; i++) {
                    System.out.print("Input Number: ");
                    a = in.nextInt();
                    outFile.println(a);
                }
            }
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        System.out.println("\nAfterSorting....");
        System.out.println(Arrays.toString(app.getSort(Arsip, n)));
    }
}


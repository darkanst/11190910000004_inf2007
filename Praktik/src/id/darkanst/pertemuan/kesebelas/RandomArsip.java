
package id.darkanst.pertemuan.kesebelas;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author Darkan
 */
public class RandomArsip {
  public void setTulis(String file, int position, String record){
      try{
          RandomAccessFile raf = new RandomAccessFile(file, "rw");
          //move file pointer to position specified
          raf.seek(position);
          //writing string to RandomAccesFile
          raf.writeUTF(record);
          raf.close();
          
      }catch (IOException e){
          System.out.println("Error : " + e.getMessage());
      }
  }
  public String getBaca(String file, int position){
      String record = "";
      try{
          RandomAccessFile raf = new RandomAccessFile(file, "r");
          //move file pointer to position specified
          raf.seek(position);
          //reading string from RandomAccessFile
          record = raf.readUTF();
          raf.close();
      }catch (IOException e){
          System.out.println("Error : "+ e.getMessage());
      }
      return record;
  }
    public static void main(String[] args) {
        String berkas = "C:\\Users\\Darkan\\Documents\\Gitlab darkanst\\temp\\random.txt";
        String data = "NIM : 123 | Nama : Budi";
        
        RandomArsip ra = new RandomArsip();
        ra.setTulis(berkas, 1, data);
        System.out.println("Tulis Berhasil");
        
        String output = ra.getBaca(berkas, 1);
        System.out.println("Baca Berhasil : " + output);
    }
}
